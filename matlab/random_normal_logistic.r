#Generates 500000X15 data with labels

set.seed(666)
x1 = rnorm(500000)           # some continuous variables 
x2 = rnorm(500000)
x3 = rnorm(500000)
x4 = rnorm(500000)           # some continuous variables 
x5 = rnorm(500000)
x6 = rnorm(500000)
x7 = rnorm(500000)           # some continuous variables 
x8 = rnorm(500000)
x9 = rnorm(500000)
x10 = rnorm(500000)           # some continuous variables 
x11= rnorm(500000)
x12 = rnorm(500000)
x13 = rnorm(500000)           # some continuous variables 
x14 = rnorm(500000)
x15= rnorm(500000)


z = 1 + 2*x1 + 3*x2 +4*x3 + 2*x4 + x5 + 2*x6 + 3*x7 +4*x8 + 2*x9 + x10 + 2*x11 + 3*x12 +4*x13 + 2*x14 + x15        # linear combination with a bias
pr = 1/(1+exp(-z))         # pass through an inv-logit function
y = rbinom(500000,1,pr)      # bernoulli response variable
#now feed it to glm:
df = data.frame(y=y,x1=x1,x2=x2,x3=x3,x4=x4,x5=x5,x6=x6,x7=x7,x8=x8,x9=x9,x10=x10,x11=x11,x12=x12,x13=x13,x14=x14,x15=x15)
#head(df)
results = glm( y~x1+x2+x3+x4+x5+x6+x7+x8+x9+x10+x11+x12+x13+x14+x15,data=df,family="binomial")
summary(results)
write.table(df, "/Users/user/Desktop/fifteendimension.txt", sep=",", row.names=FALSE)