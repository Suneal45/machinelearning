%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%     main.m
%     
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

%% Initialization
clear ; close all; clc

%% Load Data
data = load('fifteendimension.txt');
X = data(:, 2:16); % X 15 rows binomial distribution
y = data(:, 1);     % y sigmoid function, y = 1 + 2*x1 + 3*x2 +4*x3 + 2*x4 + x5 + 2*x6 + 3*x7 +4*x8 + 2*x9 + x10 + 2*x11 + 3*x12 +4*x13 + 2*x14 + x15        # linear combination with a bias


%% ============ Compute Cost and Gradient ============
%  implement the cost and gradient for logistic regression

%  Setup the data matrix appropriately, and add ones for the intercept term
[m, n] = size(X); %50000, 15

% Add intercept term to x and X_test
X = [ones(m, 1) X]; %1 added as intercept

% Initialize fitting parameters
initial_theta = zeros(n + 1, 1); %16*1
iteration =100
learning_rate = 0.00005
weight = logisticRegressionWeights( X, y, initial_theta, iteration, learning_rate)

fileID = fopen('output.txt','a');
fprintf(fileID, '\n\n');
fprintf(fileID, [num2str(iteration) '-> iterations    ' num2str(learning_rate) ' -> learning_rate']);
fprintf(fileID, '\n');


%weight= [1,2,3,4,2,1,2,3,4,2,1,2,3,4,2,1]';
fprintf(fileID,"%f, ", weight);

res = logisticRegressionClassify( X, weight );

errors = abs(y - res);
err = sum(errors)
percentage = 1 - err / size(X, 1)
%disp(percentage)
fprintf(fileID,"\n Error %f ", 1- percentage);


