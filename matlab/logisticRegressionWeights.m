%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%     logisticRegressionWeights.m
%     
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [w] = logisticRegressionWeights( XTrain, yTrain, w0, maxIter, learningRate)

    [nSamples, nFeature] = size(XTrain);
    w = w0;
    precost = 0;
    for j = 1:maxIter
        temp = zeros(nFeature,1);
        for k = 1:nSamples
%           disp("Temp" + size(temp)) %16X1
%           disp("weight" + size(w)) %16X1
%           disp("something"+ size(XTrain(k,:))) %1X16
%           disp("SIgmoid"+size(sigmoid(XTrain(k,:) * w))) % 1X1
%           disp("YtrainK" +size(yTrain(k))) %1*1
%           disp("XTrain" +size([XTrain(k,:)]')); %1X16

            temp = temp + (sigmoid(XTrain(k,:) * w) - yTrain(k)) * [XTrain(k,:)]';            
        end
        w = w - learningRate * temp;
        cost = CostFunc(XTrain, yTrain, w);
        if j~=0 && abs(cost - precost) / cost <= 0.0001
            break;
        end
        precost = cost;
    end

end

