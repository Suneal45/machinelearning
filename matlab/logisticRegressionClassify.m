%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%     logisticRegressionClassify.m
%     
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [ res ] = logisticRegressionClassify( XTest, w )

    nTest = size(XTest,1);
    res = zeros(nTest,1);
    for i = 1:nTest
        %disp(size(XTest(i,:))) 1*16
        %disp(size(w)) 16*1
        sigm = sigmoid(XTest(i,:) * w);
        if sigm >= 0.5
            res(i) = 1;
        else
            res(i) = 0;
        end
    end

end