from pyspark.mllib.classification import SVMWithSGD, LogisticRegressionModel, SVMModel, LogisticRegressionWithSGD, LogisticRegressionWithLBFGS
from pyspark.mllib.regression import LabeledPoint
from pyspark import SparkContext
from pyspark.mllib.linalg import SparseVector, Vectors  

import os
from csv import DictReader
import csv
from datetime import datetime
from math import exp, log, sqrt
import numpy as np
import mmh3
import re
import time 



D = 2 ** 18   # Hash dimension

#To run
#$SPARK_HOME/bin/spark-submit aaloo_sgdhash.py


# Load and parse the data
def parsePoint(line):
    values = [str(x) for x in line.split(' ')]
    d =' '.join(values[1:])
    return [1. if values[0].strip() == '1' else 0. , d]
    
def LabelFeature(label, feature):
    return LabeledPoint(label,feature)

def parseIndividualFeatures(feature):
      values = [mmh3.hash(x) % D for x in feature.split(' ')]
      hashedVal = {}
      indexes = []
      for index in values:
          if(index not in indexes):
              hashedVal[index] = 0
          hashedVal[index] += 1

      return hashedVal 

def label(yValue):
      return yValue

sc = SparkContext(appName="Logistic Regression")

data = sc.textFile("rcv1.train.raw.txt")
parsedData = data.map(parsePoint) #Get label and feature

#Hash Individual features
individualFeatureHash = parsedData.map(lambda r: [(label(r[0])), (parseIndividualFeatures(r[1]))]) #Use hash for individual features

#Create a sparse vector, using dictionary
sparseData = individualFeatureHash.map(lambda r: LabelFeature(r[0], Vectors.sparse(D, r[1]))) #Sparse dataset
  
start_time = time.time()
# Build the model
model = LogisticRegressionWithSGD.train(sparseData,iterations = 100) #Train
toappend = "\n-------> Total Train time:  %s seconds " % (time.time() - start_time)

start_time = time.time()

# Evaluating the model on training data
labelsAndPreds = sparseData.map(lambda p: (p.label, model.predict(p.features))) #Predict
trainErr = labelsAndPreds.filter(lambda vp: vp[0] != vp[1]).count() / float(parsedData.count()) #Training error

toappend_train_test= "\n-------> Total Train test time:  %s seconds " % (time.time() - start_time)
toappend1 ="\n----------> Training Error = " + str(trainErr)

#Save and load model
model.save(sc, "target/tmp/LogisticRegressionWithSGD100")
sameModel = LogisticRegressionModel.load(sc, "target/tmp/LogisticRegressionWithSGD100")

data = sc.textFile("rcv1.test.raw.txt")
parsedData = data.map(parsePoint)

individualFeatureHash = parsedData.map(lambda r: [(label(r[0])), (parseIndividualFeatures(r[1]))])
sparseData = individualFeatureHash.map(lambda r: LabelFeature(r[0], Vectors.sparse(D, r[1])))


start_time = time.time()
# Evaluating the model on training data
labelsAndPreds = sparseData.map(lambda p: (p.label, sameModel.predict(p.features)))
testError = labelsAndPreds.filter(lambda vp: vp[0] != vp[1]).count() / float(parsedData.count())


toappend2 = "\n-------> Total test time:  %s seconds " % (time.time() - start_time)
toappend3 = "\n----------> Test Error = " + str(testError)

with open("output", "a") as myfile:
  myfile.write('\n\n'+str(time.time())+ 'LogisticRegressionWithSGD 100 iteration')
  myfile.write(toappend)
  myfile.write(toappend_train_test)
  myfile.write(toappend1)
  myfile.write(toappend2)
  myfile.write(toappend3)
  myfile.write('\n')

