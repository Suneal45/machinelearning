from pyspark.mllib.regression import LabeledPoint
import numpy as np
from pyspark.sql import Row
from pyspark.sql import functions as sql_functions
from pyspark.sql.types import *
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from pyspark import SparkConf, SparkContext
from pyspark.sql import SQLContext

from pyspark.ml.regression import LinearRegression
from pyspark.ml.linalg import Vectors, VectorUDT
from pyspark.sql.functions import udf
import time

#Create a file for the sketch matrix
filename_sketch = 'sketched_training_year_synthetic_50000.csv'
filename_original = 'train_synthetic90.csv'
filename_test = 'test_synthetic10.csv'
sketch_size = 50000	

#Load training data
m =np.genfromtxt(filename_original, delimiter=',')

# In-Memory Sketching function
# s = size of sketch
# matrixA = original matrix
def countSketchInMemory(matrixA, s):
  m, n = matrixA.shape
  # Create matrix of required dimension
  matrixC = np.zeros([m, s])
  # Hash each column with discrete value randomly sampled from {1...s}
  hashedIndices = np.random.choice(s, n, replace=True)
  # Generate random sample for {+1, -1} in the given sample of size n
  randSigns = np.random.choice(2, n, replace=True) * 2 - 1 # a n-by-1{+1, -1} vector
  # Flip the signs of 50% columns of A
  matrixA = matrixA * randSigns.reshape(1, n) 
  for i in range(s):
    # Based on the value of i set elements of list as true or false
    idx = (hashedIndices == i)
    #Sum horizontally
    matrixC[:, i] = np.sum(matrixA[:, idx], 1)
  return matrixC

#Take transpose of the matrix
tra=m.T
print(tra.shape)
#This will output size of the matrix as mXs
matrixC = countSketchInMemory(tra, sketch_size)

print("--> Shape of Output Matrix")
print(matrixC.T.shape)

#Compare the l2 norm between original matrix and the sketched matrix

rowNormsA = np.sqrt(np.sum(np.square(tra), 1))
print(rowNormsA)

rowNormsC = np.sqrt(np.sum(np.square(matrixC), 1))
print(rowNormsC)

#Save the sketched matrix to a file.
np.savetxt(filename_sketch, matrixC.T, delimiter=',')



#Run spark to train on sketched Matrix and test it on original test dataset

sc = SparkContext(appName="LinearRegression")
sc.setLogLevel("WARN")
sqlContext = SQLContext(sc)

from pyspark.ml.evaluation import RegressionEvaluator
evaluator = RegressionEvaluator(predictionCol = 'prediction')


def runModel(filename):
  raw_data = sqlContext.read.load(filename, 'text')
  num_points = raw_data.count()

  print("Number of observations" +str(num_points))


  attribute_description = "90 attributes, 12 = timbre average, 78 = timbre covariance. \nThe first value is the year (target), ranging from 1922 to 2011. \nFeatures extracted from the 'timbre' features from The Echo Nest API. \nWe take the average and covariance over all 'segments', each segment being described by a 12-dimensional timbre vector."

  #Create DataFrame from RDD
  df = raw_data.rdd.map(lambda row: str(row['value']).split(",")).map(lambda row: LabeledPoint(row[0], [float(x) for x in row[1:]])).toDF(["Features", "Year"])

  #Group by Year
  year_data = df.select("Year").groupBy("Year").count()
  print(df.select("Year").show())
  year_data = year_data.toPandas()

  #Get Average Year
  avg_year = year_data["Year"]
  average_year = sum(avg_year)/len(avg_year)

  df.cache()

  parsed_train_data_df = df.rdd.map(lambda row: (Vectors.dense(row["Features"]), float(row['Year'])))
  parsed_train_data_df = sqlContext.createDataFrame(parsed_train_data_df,["features","label"])

  #Linear regression model parameter values
  num_iters = 1000  # iterations
  reg = 1e-1  # regParam
  alpha = .2  # elasticNetParam
  use_intercept = True  # intercept

  #Run Linear Regression
  lin_reg = LinearRegression(maxIter = num_iters, regParam = reg, elasticNetParam = alpha, fitIntercept = use_intercept, labelCol = 'label', featuresCol = 'features')
  start_time=time.time()
  first_model = lin_reg.fit(parsed_train_data_df)
  print("Training time for " + str(filename) + str(time.time() - start_time))
  
  coeffs_LR1 = first_model.coefficients
  intercept_LR1 = first_model.intercept
  print(coeffs_LR1, intercept_LR1)
  return first_model

model = runModel(filename_sketch)
second_model =  runModel(filename_original)

# Run the model on Test dataset 
raw_data = sqlContext.read.load(filename_test, 'text')
num_points = raw_data.count()

print("Num Points test")
print(num_points)
df = raw_data.rdd.map(lambda row: str(row['value']).split(",")).map(lambda row: LabeledPoint(row[0], [float(x) for x in row[1:]])).toDF(["Features", "Year"])
df.cache()

#pyspark

parsed_val_data_df = df.rdd.map(lambda row: (Vectors.dense(row["Features"]), float(row['Year'])))
parsed_val_data_df = sqlContext.createDataFrame(parsed_val_data_df,["features","label"])

#parsed_val_data_df = parsed_val_data_df.withColumn("label", parsed_val_data_df["label"].cast(DoubleType()))
val_pred_df = model.transform(parsed_val_data_df)
val_pred_df2 = second_model.transform(parsed_val_data_df)

rmse_val_LR1 = evaluator.evaluate(val_pred_df,  {evaluator.metricName: "rmse"})
rmse_val_LR2 = evaluator.evaluate(val_pred_df2 , {evaluator.metricName: "rmse"})
print ('Validation RMSE:LR1 = ',  rmse_val_LR1)
print ('Validation RMSE:LR Original= ',  rmse_val_LR2)


'''
Map Reduce implementation

raw_data = sqlContext.read.load('yearmsd_train.csv', 'text')
num_points = raw_data.count()

print attribute_description
print '\nNumber of data points: ', num_points, "\n"

def countSketchMapReduce(filepath, s):
    # load data
    rawRDD = sc.textFile(filepath)
    # parse string data to vectors
    vectorRDD = rawRDD.map(lambda l: np.asfarray(l.split(",")))
    num_points = vectorRDD.count()
    print("===>")
    # map the vectors to key-value pairs
    pairRDD = rawRDD.map(lambda vect: (np.random.randint(0, s), (np.random.randint(0, 2) * 2 - 1) * vect ))
    
    #reducer
    vectList = pairRDD.reduceByKey(lambda v1, v2: v1+v2).map(lambda pair: pair[1]).collect()
    thefile = open('test_vect.txt', 'w')
    for item in vectList:
      thefile.write("%s\n" % item)
    #return np.asarray(vectList).T
    return

s = 10000 # sketch size, can be tuned
filepath = 'yearmsd_training.csv'
matrixC = countSketchMapReduce(filepath, s)
np.savetxt('sketched_training_map_reduce.csv', matrixC, delimiter=',') 
#matrixC = np.asarray(matrixC_vect).T
#print(matrixC_vect.shape)
#matrixC_string = np.array_str(matrixC)


#df = matrixC-string.map(lambda row: str(row['value']).split(",")).map(lambda row: LabeledPoint(row[0], [float(x) for x in row[1:]])).toDF(["Features", "Year"])
#print(df.select("Year").show())
'''
