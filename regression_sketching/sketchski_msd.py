from sklearn import datasets, linear_model, metrics
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import PolynomialFeatures
import math, scipy, numpy as np
from scipy import linalg
from sklearn.datasets import make_regression
import time

# X, Y = make_regression(n_samples=100000, n_features=30, n_informative=3, n_targets=1, noise=2.0)
# from sklearn.datasets import dump_svmlight_file
# dump_svmlight_file(X,Y, 'regresion100000x30.txt')
# np.savetxt('regresion100000x30.csv', d, delimiter=',')   # X is an array

training_data =np.genfromtxt('yearmsd_training.csv',delimiter=',')
test_data =np.genfromtxt('yearmsd_test.csv',delimiter=',')
print("Train/Test: " + str(training_data.shape) + " / " + str(test_data.shape))     
y_train = training_data[:,:1]
x_train = training_data[:,1:]

y_test = test_data[:, :1]
x_test = test_data[:, 1:]

start_time = time.time()
regr1 = linear_model.LinearRegression()
regr1.fit(x_train, y_train)
print("Training time for default  dataset " + str(time.time() - start_time))

pred = regr1.predict(x_test)
def regr_metrics(act, pred):
  return (math.sqrt(metrics.mean_squared_error(act, pred)), metrics.mean_absolute_error(act, pred))
print("Original Dataset")
print(regr_metrics(y_test, regr1.predict(x_test)))


training_data = np.column_stack((y_train,x_train))
transposed = training_data.T

def countSketchInMemroy(matrixA, s):
  m, n = matrixA.shape
  matrixC = np.zeros([m, s])
  hashedIndices = np.random.choice(s, n, replace=True)
  randSigns = np.random.choice(2, n, replace=True) * 2 - 1 # a n-by-1{+1, -1} vector
  matrixA = matrixA * randSigns.reshape(1, n) # flip the signs of 50% columns of A
  for i in range(s):
    idx = (hashedIndices == i)
    matrixC[:, i] = np.sum(matrixA[:, idx], 1)
  return matrixC

matrixC = countSketchInMemroy(transposed, 50000)
rowNormsA = np.sqrt(np.sum(np.square(transposed), 1))
print(rowNormsA)

rowNormsC = np.sqrt(np.sum(np.square(matrixC), 1))
print(rowNormsC)

print(matrixC.shape)
sketched_matrix = matrixC.T

y_sketched = sketched_matrix[:, :1]
x_sketched = sketched_matrix[:, 1:]

print(y_sketched.shape)
print(x_sketched.shape)
print()
print(y_test.shape)
print(x_test.shape)

start_time = time.time()
regr = linear_model.LinearRegression()
regr.fit(x_sketched, y_sketched)
print("Training time for sketch  dataset " + str(time.time() - start_time))

pred = regr.predict(x_test)
def regr_metrics(act, pred):
  return (math.sqrt(metrics.mean_squared_error(act, pred)), metrics.mean_absolute_error(act, pred))

print("Sketched Dataset")
print(regr_metrics(y_test, regr.predict(x_test)))

'''
#testing sketched model

m =np.genfromtxt('YearPredictionMSD.txt',delimiter=',')
m.shape

Y = m[:,:1]
X = m[:,1:]

X.shape
Y.shape

d = np.column_stack((Y,X))
d.shape #100000,31

trn,test,y_trn,y_test = train_test_split(X, Y, test_size=0.1)
trn.shape, test.shape

pred = regr.predict(test)
def regr_metrics(act, pred):
  return (math.sqrt(metrics.mean_squared_error(act, pred)),metrics.mean_absolute_error(act, pred))
print("Sketched model on real dataset")
print(regr_metrics(y_test, regr.predict(test)))
'''
