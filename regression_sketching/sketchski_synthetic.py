from sklearn import datasets, linear_model, metrics
from sklearn.model_selection import train_test_split
import math, scipy, numpy as np
from sklearn.datasets import make_regression
import time 

filename = 'regression_synthetic_csv.csv'

X, Y = make_regression(n_samples=515345, n_features=90, n_informative=90, n_targets=1, noise=2.0)
d = np.column_stack((Y,X))
np.savetxt(filename, d, delimiter=',')   # X is an array
#dump_svmlight_file(X,Y, 'regression_synthetic.csv')

m =np.genfromtxt(filename, delimiter=',')
#m.shape
    
Y = m[:,:1]
X = m[:,1:]

trn,test,y_trn,y_test = train_test_split(X, Y, test_size=0.1)
trn.shape, test.shape

train_dump = np.column_stack((y_trn, trn))
test_dump = np.column_stack((y_test,test))
np.savetxt('train_synthetic90.csv', train_dump, delimiter=',')  
np.savetxt('test_synthetic10.csv', test_dump, delimiter=',')  


start_time = time.time()
regr1 = linear_model.LinearRegression()
regr1.fit(trn, y_trn)
print("Training time for default  dataset " + str(time.time() - start_time))
pred = regr1.predict(test)
def regr_metrics(act, pred):
  return (math.sqrt(metrics.mean_squared_error(act, pred)),
metrics.mean_absolute_error(act, pred))
print("Original Dataset")
print(regr_metrics(y_test, regr1.predict(test)))


## Load sketched dataset and apply test it on test dataset
training_synthetic = 'train_synthetic90.csv'
m =np.genfromtxt(training_synthetic,delimiter=',')
test_from_file = np.genfromtxt('test_synthetic10.csv',delimiter=',')

tra=m.T
tra.shape

def countSketchInMemroy(matrixA, s):
  m, n = matrixA.shape
  matrixC = np.zeros([m, s])
  hashedIndices = np.random.choice(s, n, replace=True)
  randSigns = np.random.choice(2, n, replace=True) * 2 - 1 # a n-by-1{+1, -1} vector
  matrixA = matrixA * randSigns.reshape(1, n) # flip the signs of 50% columns of A
  for i in range(s):
    idx = (hashedIndices == i)
    matrixC[:, i] = np.sum(matrixA[:, idx], 1)
  return matrixC

matrixC = countSketchInMemroy(tra, 50000)
print("Comparing L2 norm between training data and sketched data")
rowNormsA = np.sqrt(np.sum(np.square(tra), 1))
print(rowNormsA)

rowNormsC = np.sqrt(np.sum(np.square(matrixC), 1))
print(rowNormsC)
matrixC = matrixC.T
y_trn = matrixC[:,:1]
trn= matrixC[:,1:]


y_tst = test_from_file[:,:1]
tst = test_from_file[:, 1:]

start_time = time.time()
regr1 = linear_model.LinearRegression()
regr1.fit(trn, y_trn)
print("Training time for sketching  dataset " + str(time.time() - start_time))
pred = regr1.predict(tst)
def regr_metrics(act, pred):
  return (math.sqrt(metrics.mean_squared_error(act, pred)), metrics.mean_absolute_error(act, pred))
print("Sketched Dataset")
print("RMSE", "MSE")
print(regr_metrics(y_tst, regr1.predict(tst)))
