import os
from csv import DictReader
import csv
from datetime import datetime
from math import exp, log, sqrt
import numpy as np
import mmh3
import re
import time 

# number of weights use for learning changing it to 24 has slight effect because of reduced collision
D = 2 ** 18  
logbatch = 100000 #Print current prediction result
lambda1 = 0.00
lambda2 = 0.00

alpha = .5   # learning rate for sgd optimization
adapt = 1.   # Use adagrad, sets it as power of adaptive factor. >1 will amplify adaptive measure and vice versa
fudge = 0.5  # Fudge factor

w = [0.] * D  # weights

g = [fudge] * D  # sum of historical gradients

# start training a logistic regression model using one pass sgd
loss = 0.
lossb = 0.
header = ['Label','Others']

# A. Bounded logloss
# INPUT:
#     p: our prediction
#     y: real answer
# OUTPUT
#     logarithmic loss of p given y
def logloss(p, y):
    p = max(min(p, 1. - 10e-17), 10e-17) #0.000000000001 used as bounds
    #return- (sum(y * log(p) + (1 - y) * log(1 - p))) 
    return -log(p) if y == 1. else -log(1. - p)


#  Prediction
# INPUT:
#     x: Input feature
#     w: weight value
# OUTPUT
#     Sigmoid output probability of p(y=1| x;w)
def get_p(x, w):
    wTx = 0.
    for i, xi in x.items():
        wTx += w[i] * xi  

    return 1. / (1. + exp(-max(min(wTx, 50.), -50.)))  # bounded sigmoid


# Update given model
# INPUT:
#     w: weights
#     n: a counter that counts the number of times we encounter a feature
#        this is used for adaptive learning rate
#     x: feature
#     p: prediction of our model
#     y: answer
# OUTPUT:
#     w: updated model
#     g: gradient squared
def update_w(w, g, x, p, y):
    for i, xi in x.items():
        # alpha / (sqrt(g) + 1) is the adaptive learning rate heuristic
        # (p - y) * x[i] is the current gradient
        # note that in our case, if i in x then x[i] = 1
        #We're not doing regularization
        delreg = (lambda1 * ((-1.) if w[i] < 0. else 1.) + lambda2 * w[i]) if i != 0 else 0.
        delta = (p - y) * xi + delreg
        if adapt > 0:
            g[i] += delta ** 2
        w[i] -= delta * alpha / ((sqrt(g[i])) ** adapt)  # Minimising log loss, adapt = 1
                                                        #1 is smoothing term that avoids dividing by 0
    return w, g


errorCount = 0
start_time = time.time()
reader = csv.reader(open('data/rcv1.train.txt'), delimiter=' ')
for t, line in enumerate(reader):
    y = 1. if line[0].strip() == '1' else 0. 
    x = {}
    for item in line[1:]:
        splitInput = item.split(':')
        x[int(splitInput[0])] = float(splitInput[1])

    p = get_p(x, w)
               
    if((p > 0.5 and y==0.) or (p <= 0.5 and y==1.)):
        errorCount +=1
    lossx = logloss(p, y)
    loss += lossx
    lossb += lossx
            #pnerr += 1. if (p * y <= 0) else 0
    if (t % logbatch == 0 or t==781264) and t > 1:
       print('%s\tencountered: %d\tcurrent whole logloss: %f\tcurrent batch logloss: %f\t and finally real loss %f' % (datetime.now(), t, loss/t, lossb/logbatch, errorCount/t))
       lossb = 0.

    w, g = update_w(w, g, x, p, y)

print("Total train time:  %s seconds " % (time.time() - start_time))

print("\n")

errorCount = 0
loss = 0.
lossb = 0.
lossx = 0.

start_time = time.time()
reader = csv.reader(open('data/rcv1.test.txt'), delimiter=' ')
for t, line in enumerate(reader):
    y = 1. if line[0].strip() == '1' else 0. 
    x = {}
    for item in line[1:]:
        splitInput = item.split(':')
        x[int(splitInput[0])] = float(splitInput[1])
    p = get_p(x, w)

    if((p > 0.5 and y==0.) or (p <= 0.5 and y==1.)):
        errorCount +=1
    lossx = logloss(p, y)
    loss += lossx
    lossb += lossx

    if (t % 2000 == 0 or t==23147) and t > 1:
       print('%s\tencountered: %d\tcurrent whole logloss: %f\tcurrent batch logloss: %f\t and finally real loss %f' % (datetime.now(), t, loss/t, lossb/logbatch, errorCount/t))
       lossb = 0.
print("Total test time:  %s seconds " % (time.time() - start_time))


