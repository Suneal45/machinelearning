from __future__ import print_function
from pyspark.mllib.classification import SVMWithSGD, LogisticRegressionModel, SVMModel, LogisticRegressionWithSGD, LogisticRegressionWithLBFGS
from pyspark.mllib.regression import LabeledPoint
from pyspark import SparkContext
from pyspark.mllib.linalg import SparseVector  
from pyspark.mllib.util import MLUtils
import os
from csv import DictReader
import csv
from datetime import datetime
from math import exp, log, sqrt
import numpy as np
import mmh3
import re
import time 



D = 2 ** 18   # number of weights use for learning
miniBatchFraction = 1
stepSize = 1 
convergenceTolerance = 0.00001
iterationsUsed =100
dataset='hashed'

#Context
sc = SparkContext(appName="LogisticRegressionWithSGD RCV Default")

#Training data
sparseData = MLUtils.loadLibSVMFile(sc,"/Users/user/data/hashed_train.txt" , multiclass=False, numFeatures=D, minPartitions=None)

start_time = time.time()

# Build the model
model = SVMWithSGD.train(sparseData,iterations = iterationsUsed, step=stepSize, convergenceTol=convergenceTolerance, miniBatchFraction= miniBatchFraction)

toappend = "\n------->Total Train time:  %s seconds " % (time.time() - start_time)

start_time = time.time()

# Evaluating the model on training data
labelsAndPreds = sparseData.map(lambda p: (p.label, model.predict(p.features)))
trainErr = labelsAndPreds.filter(lambda vp: vp[0] != vp[1]).count() / float(sparseData.count())

toappend_train_test= "\n------->Total Train test time:  %s seconds " % (time.time() - start_time)
toappend1 ="\n---------->Training Error = " + str(trainErr)

#This creates files, datetime added so that future run doesn't give error. Might have to delete the folder if not using it.
datetoday = datetime.now().strftime("%m%d%s")

#Save and load model
model.save(sc, "target/tmp/LogisticRegressionModell-" + datetoday )
sameModel = SVMModel.load(sc, "target/tmp/LogisticRegressionModell-" + datetoday)

#Loading Test Dataset.
sparseData = MLUtils.loadLibSVMFile(sc,"/Users/user/data/hashed_test.txt" ,multiclass=False, numFeatures=262144, minPartitions=None)
start_time = time.time()

# Evaluating the model on training data
labelsAndPreds = sparseData.map(lambda p: (p.label, sameModel.predict(p.features)))
testError = labelsAndPreds.filter(lambda vp: vp[0] != vp[1]).count() / float(sparseData.count())

toappend2 = "\n------->Total test time:  %s seconds " % (time.time() - start_time)
toappend3 = "\n---------->Test Error = " + str(testError)

with open("rcv_output.txt", "a") as myfile:
  myfile.write(' \n\n' +str(datetime.now().strftime("%m%d"))+ ' SVMWithSGD ' +dataset +  ' step: ' + str(stepSize) + ' iterations: ' +str(iterationsUsed) + ' miniBatch: '+ str(miniBatchFraction))
  myfile.write(toappend)
  myfile.write(toappend_train_test)
  myfile.write(toappend1)
  myfile.write(toappend2)
  myfile.write(toappend3)
  myfile.write('\n')