#Uses SGD Classidier and performs hyperparameter tuning on SVMLight dataset
#Error percent is around 5% for both dataset already converted to hash and 
#the default dataset generated through TF-IDF and bag of words model
#Change training and test data to hashed_train.txt and hashed_test.txt


from sklearn.externals.joblib import Memory
from sklearn.datasets import load_svmlight_file
import numpy as np
from sklearn import linear_model
import time

#mem = Memory("./mycache")

#@mem.cache
def get_data(filepath):
    data = load_svmlight_file(filepath)
    return data[0], data[1]

load_time= time.time()
X_train, y_train = get_data("data/rcv1.train.txt") #train
iterations = [1,5,10]
alpha_float = 0.0001
loss_str='squared_hinge'

X_test, y_test = get_data("data/rcv1.test.txt") #test
load_time = time.time()- load_time

with open("scikit_rcv_iterations.txt", "a") as myfile:
	myfile.write("\n----------------Original Dataset Without hash--------------------\n")
	for r in iterations:
		start_time = time.time()
		clf = linear_model.SGDClassifier(loss=loss_str, alpha =alpha_float, n_iter=r, n_jobs=-1)
		clf.fit(X_train, y_train)
		train_time = time.time() - start_time
		start_time = time.time()
		testing = clf.score(X_test,y_test)
		test_time = time.time() - start_time
		myfile.write("\n\n  Loss:" + loss_str + "  Iteration: "+ str(r) + "\t" + "  alpha: " +str(alpha_float) +  "\n")
		myfile.write("\nTrain Error:\t" + str(1 -clf.score(X_train,y_train)) + "  Test Error: \t" +str(1 -testing))
		myfile.write("\nTrain Time:\t" + str(train_time) + "  Test Time: \t" +str(test_time))
		myfile.write("\n Load datasets time:  " + str(load_time))



